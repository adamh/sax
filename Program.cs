﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

/*Args
 [0]Window Size
 [1]Word Size
 [2]Numerosity Reduction: 1 - No reduction, 2 - Record iff previous different from current
  * Input:
 data              is the raw time series. 
 N                 is the length of sliding window (use the length of the raw time series
                   instead if you don't want to have sliding windows)
 n                 is the number of symbols in the low dimensional approximation of the sub sequence.
 alphabet_size     is the number of discrete symbols. 2 <= alphabet_size <= 10, although alphabet_size = 2 is a 
  special "useless" case.
 
 win_size is the number of data points to compress to a single symbol or step
 wordsize is how big the 'code' for the window size will be, the number of symbols to use for encoding, must be greater than 2
            
 */

namespace SAX_Test
{
    internal class Program
    {

        private static void Main(string[] args)
        {

            /*        
             SAX sax;
#if !DEBUG
            if (args.Length < 1)
            {
                Console.WriteLine("Not enough arguments");
                Console.ReadLine();
                return;
            }
            int Word = Convert.ToInt32(setting[0]);
            int Alpha = Convert.ToInt32(setting[1]);
            
            if(Alpha < 3 || Alpha > 12)
            {
                Console.WriteLine("Alphabet size is outside of supported range (3 - 12)")
                return;
            }
            sax = new SAX(Word, Alpha);
#endif
             */
            /*
            sax = new SAX(3, 5);

            string[] text = File.ReadAllLines("test_data.txt");

            //Read in a series of values into a list
            List<double> data = text.Select(Convert.ToDouble).ToList();

            string letters = sax.ToLetterRep(new List<double>() {7, 1, 4, 4, 4, 4, 1, 2, 3, 5});
            string textFileReadout = sax.ToLetterRep(data);

            Console.WriteLine(letters);
            Console.WriteLine(textFileReadout);
            */
            /*
            foreach (double c in data)
            {
                Console.WriteLine(c.ToString());
            }
            
            File.WriteAllLines("output.txt", data.Select(d => d.ToString()).ToArray());
            */
            Console.ReadKey();

        }


    }
}


