﻿
using System.Collections.Generic;
using NUnit.Framework;

namespace SAX_Test
{
    [TestFixture]
    public class SAXTest
    {
        [Test]
        public void TestToLetterRep()
        {
            //Default Constructor, Word Size = 6, AlphabetSize = 5, (abcde)
            SAX s = new SAX();
            string letters = s.ToLetterRep(new List<double>() {7, 1, 4, 4, 4, 5});
            StringAssert.AreEqualIgnoringCase("eacccd", letters);
            letters = s.ToLetterRep(new List<double>(){1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,6,6,6,6,10,100});
            StringAssert.AreEqualIgnoringCase("bbbbce", letters);
        }
    }
}
