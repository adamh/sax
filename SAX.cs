﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SAX_Test
{
    public class SAX
    {
        private const int AlphabetStart = (int) 'a'; //97
        public int WordSize { get; set; }
        public int AlphabetSize { get; set; }
        private double scaling = 1;
        private List<Indices> indexes = new List<Indices>();

        private readonly Dictionary<int, List<double>> Breakpoints = new Dictionary<int, List<double>>()
        {
            {3, new List<double>() {-0.43, 0.43}},
            {4, new List<double>() {-0.67, 0, 0.67}},
            {5, new List<double>() {-0.84, -0.25, 0.25, 0.84}},
            {6, new List<double>() {-0.97, -0.43, 0, 0.43, 0.97}},
            {7, new List<double>() {-1.07, -0.57, -0.18, 0.18, 0.57, 1.07}},
            {8, new List<double>() {-1.15, -0.67, -0.32, 0, 0.32, 0.67, 1.15}},
            {9, new List<double>() {-1.22, -0.76, -0.43, -0.14, 0.14, 0.43, 0.76, 1.22}},
            {10, new List<double>(){-1.28, -0.84, -0.52, -0.25, 0, 0.25, 0.52, 0.84, 1.28}},
            {11, new List<double>(){-1.34, -0.91, -0.6, -0.35, -0.11, 0.11, 0.35, 0.6, 0.91, 1.34}},
            {12, new List<double>(){-1.38, -0.97, -0.67, -0.43, -0.21, 0, 0.21, 0.43, 0.67, 0.97, 1.38}}
        };

        public SAX()
        {
            //Default word and alphabet size
            WordSize = 6;
            AlphabetSize = 5;
        }
        /// <summary>
        /// Constructor that takes in word and alphabet size
        /// </summary>
        /// <param name="word">Word size for representative string of dataset</param>
        /// <param name="alpha">Minimum is 3, maximum is 12</param>
        public SAX(int word, int alpha)
        {
            WordSize = word;
            AlphabetSize = alpha;
        }

        // Convert a normalized value of doubles to Piecewise Aggregate Approximation
        private List<double> ConvertToPAA(List<double> normalizedList)
        {
            int len = normalizedList.Count();
            //divide word size into array to see how many sections we will have representing the full list of values
            double stepFloat = (len/(float)WordSize);
            int step = (int)(Math.Ceiling(stepFloat));
            var approximationValues = new List<double>();

            int i = 0;
            int start = 0;

            while (start <= (len - step))
            {
                List<double> subsection = normalizedList.GetRange(start, step);
                approximationValues.Add(subsection.Average());
                indexes.Add(new Indices {StartPos = start, StartStep = (start + step)});
                i += 1;
                start = (int)(i*stepFloat);
            }
            
            return approximationValues;
        }

        private List<string> Alphabetize(List<double> paaList)
        {
            List<double> Levels = Breakpoints[AlphabetSize];
            var alphabetizedList = new List<string>();

            for (int i = 0; i < paaList.Count; i++)
            {
                bool letterFound = false;

                for (int j = 0; j < Levels.Count; j++)
                {
                    if (paaList[i] < Levels[j])
                    {
                        alphabetizedList.Add(Convert.ToChar(AlphabetStart+j).ToString());
                        letterFound = true;
                        break;
                    }
                }
                if (!letterFound)
                {
                    alphabetizedList.Add(Convert.ToChar(AlphabetStart + Levels.Count).ToString());
                }
            }
            return alphabetizedList;
        }

        public string ToLetterRep(List<double> ListToLetters)
        {
            //Normalize the Data
            ListToLetters = NormalizeList(ListToLetters);

            //Convert to PAA list
            ListToLetters = ConvertToPAA(ListToLetters);

            List<string> returnList = Alphabetize(ListToLetters);

            string letterRep = "";
            foreach (var str in returnList)
            {
                letterRep += str;
            }
            return letterRep;

        }
        //Normalize a list of doubles
        private List<double> NormalizeList(List<double> list2norm)
        {
            var avgOfList = list2norm.Average();
            var stdOfList = StdDeviation(list2norm);

            return list2norm.Select(x => (x - avgOfList)/stdOfList).ToList();
        }

        //Calculate Standard Deviation for a list of doubles
        private double StdDeviation(List<double> stdList)
        {
            var avg = stdList.Average();
            var sumOfSquaresDiff = stdList.Select(val => (val - avg)*(val - avg)).Sum();
            var stdDev = Math.Sqrt(sumOfSquaresDiff/stdList.Count);
            return stdDev;
        }
    }

    public class Indices
    {
        public int StartPos { get; set; }
        public int StartStep { get; set; }
    }
}

